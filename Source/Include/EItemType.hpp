#ifndef SYSTEMINVENTORY_EITEMTYPE_HPP
#define SYSTEMINVENTORY_EITEMTYPE_HPP

enum EItemType
{
    // Any object of this type must
    // be treated as a null object.
            NONE = 0,

    SHIELD
};

#endif //SYSTEMINVENTORY_EITEMTYPE_HPP
